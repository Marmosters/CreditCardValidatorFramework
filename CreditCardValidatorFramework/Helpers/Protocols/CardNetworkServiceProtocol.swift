//
//  CardNetworkServiceProtocol.swift
//  CreditCardValidatorFramework
//
//  Created by Vladimir. Evstratov on 12/26/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

protocol CardNetworkServiceProtocol {
    var url: URL? {get set}
    func getData(completion: ((PaymentCardModel?, Error?) -> Void)?)
}

extension CardNetworkServiceProtocol {
    func getData(completion: ((PaymentCardModel?, Error?) -> Void)?) {
        guard let url = url else {
            completion?(nil, CVError.badURL)
            return
        }
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { data, response, error in
            guard error == nil, let data = data else {
                completion?(nil, error)
                return
            }
            
            guard let response = response, let mime = response.mimeType, mime == "application/json" else {
                completion?(nil, CVError.wrongMIMEType)
                return
            }
            
            do {
                let result = try JSONDecoder().decode(PaymentCardModel.self, from: data)
                completion?(result, nil)
            } catch {
                completion?(nil, error)
            }
        }
        task.resume()
    }
}
