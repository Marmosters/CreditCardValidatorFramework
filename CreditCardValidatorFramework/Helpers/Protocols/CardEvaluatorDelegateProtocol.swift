//
//  CardEvaluatorDelegateProtocol.swift
//  CreditCardValidatorFramework
//
//  Created by Vladimir on 28.12.2019.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

public enum CardEvaluationResult {
    case valid, invalid, undefined
}

public protocol CardEvaluatorDelegate: class {
    /// - description
    /// notify delegate about  starting  fetchung issuer data
    func cardDetailsWillUpdated()
    
    ///- description
    /// notify delegate about  finishing fetching issuer data
    /// - Parameters
    /// `details` - Issuer data
    /// `error` - fetch error
    ///  Possible errors:
    ///  - Network provided erros
    ///  - `CVError.badURL` - issus wit building request url
    ///  - `CVError.wrongMIMEType` - server returnned somthing, but not JSON
    func cardDetailsDidUpdated(_ details: PaymentCardModel?, _ error: Error?)
    
    ///- description
    /// notify delegate about  number evaluation results
    /// - Parameters
    /// `result` - validation result
    ///    Possible values:
    ///    - `CardValidationResult.valid` - evaluation passed
    ///    - `CardValidationResult.invalid` - evaluation failed
    ///    - `CardValidationResult.undefined` - nothing to evaluate
    /// `error` - clarification of negative evaluation
    ///  Possible errors:
    ///  - `CVError.invalidNumberLength` - provided string is not a card number becouse ot lenth
    ///  - `CVError.invalidCardNumber` - provided strinf did not passed Luhn evaluation
    ///  - `CVError.wrongEntry` - provided string conrains restricted characters. **Only digits and spaces are allowed**
    ///  - `CVError.zeroEntry` - provided string starts with 0 or space. **Leading 0 and spaces are not allowed**

    func cardNumberDidValidated(_ result: CardEvaluationResult, _ error: Error?)
}

public extension CardEvaluatorDelegate {
    func cardDetailsWillUpdated() {}
}
