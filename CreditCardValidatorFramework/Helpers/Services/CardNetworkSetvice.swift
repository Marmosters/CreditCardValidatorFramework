//
//  CardNetworkSetvice.swift
//  CreditCardValidatorFramework
//
//  Created by Vladimir. Evstratov on 12/26/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

class CardNetworkSetvice: CardNetworkServiceProtocol {
    var url: URL?
    let apiUrl = "https://lookup.binlist.net/"
    
    func checkData(emitemtCode: String, completion: ((PaymentCardModel?, Error?) -> Void)? ) {
        url =  URL(string: apiUrl + emitemtCode)
        DispatchQueue.global(qos: .userInitiated).async {
            self.getData { result, error in
                DispatchQueue.main.async {
                    completion?(result, error)
                }
            }
        }
    }
}
