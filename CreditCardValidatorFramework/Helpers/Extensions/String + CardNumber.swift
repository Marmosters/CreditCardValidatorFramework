//
//  String + CardNumber.swift
//  CreditCardValidatorFramework
//
//  Created by Vladimir on 28.12.2019.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

typealias CardNumber = String

extension CardNumber {
    var toNumberString: CardNumber? {
        let result  = self.replacingOccurrences(of: " ", with: "")
        let regExp = try? NSRegularExpression(pattern: "^([1-9][0-9]*)$")
        let range = NSRange(location: 0, length: result.count)
        return regExp?.numberOfMatches(in: result, options: .anchored, range: range) ?? 0 > 0 ? result :  nil
    }
    
    var issuerCode: CardNumber? {
        guard let number = self.toNumberString, number.count >= CardEvaluator.issuerCodeLenght else {
            return nil
        }
        return number[0..<CardEvaluator.issuerCodeLenght]
    }
    
    subscript(_ range: CountableRange<Int>) -> String {
        let idx1 = index(startIndex, offsetBy: max(0, range.lowerBound))
        let idx2 = index(startIndex, offsetBy: min(self.count, range.upperBound))
        return String(self[idx1..<idx2])
    }
}
