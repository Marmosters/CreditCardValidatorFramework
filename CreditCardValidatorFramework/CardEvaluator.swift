//
//  Validator.swift
//  CreditCardValidatorFramework
//
//  Created by Vladimir. Evstratov on 12/24/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

public class CardEvaluator {
    /// - description:
    /// delegate receives number evaluation status
    public weak var delegate: CardEvaluatorDelegate?
    
    /// - Returns: Expected card number length
    public var validNumberLehgths: ClosedRange<Int> {
        var range: ClosedRange<Int> = validNunberDefaultRane
        if let length = cardDetails?.numberDetails?.length {
            range = length...length
        }
        return range
    }
    
    /// - Description:
    /// This  variable acceps string entry to be evalueted as card number.
    /// Evaluation starts  by value assigment and result will be delivered via
    /// CardValidatorDelegate protocol methods.
    public var cardNumber: String? {
        get {
            return self.processingNumber
        }
        set {
            guard newValue?.first != " " && newValue?.first != "0" else {
                delegate?.cardNumberDidValidated(.invalid, CVError.zeroEntry)
                return
            }
            if let number = newValue?.toNumberString {
                updateIssuer(number)
                self.processingNumber = number
                validateNumber(number)
            } else {
                newValue?.isEmpty ?? true ? delegate?.cardNumberDidValidated(.undefined, nil) : delegate?.cardNumberDidValidated(.invalid, CVError.wrongEntry)
            }
        }
    }
    
    static let issuerCodeLenght = 6
    let issuerServce = CardNetworkSetvice()
    var issuerUpdateInProgress = false
    private let validNunberDefaultRane = 12...19
    private var cardDetails: PaymentCardModel?
    private var processingNumber: String = ""
    
    
    public init() {}
    
    private func updateIssuer(_ number: CardNumber) {
        guard let issuerNumber = number.issuerCode  else {
            cardDetails = nil
            delegate?.cardDetailsDidUpdated(nil, nil)
            return
        }
        guard !issuerUpdateInProgress else {return}
        if issuerNumber != processingNumber.issuerCode || cardDetails == nil {
            delegate?.cardDetailsWillUpdated()
            issuerUpdateInProgress = true
            issuerServce.checkData(emitemtCode: issuerNumber) { [weak self] details, error in
                self?.issuerUpdateInProgress = false
                guard self?.processingNumber.issuerCode == issuerNumber else {return}
                self?.cardDetails = details
                self?.delegate?.cardDetailsDidUpdated(details, error)
            }
        }
    }
    
    private func validateNumber(_ number: CardNumber) {
        guard !number.isEmpty else {
            delegate?.cardNumberDidValidated(.undefined, nil)
            return
        }
        guard validNumberLehgths.contains(number.count) else {
            delegate?.cardNumberDidValidated(.invalid, CVError.invalidNumberLength)
            return
        }
        let revesedNum = number.reversed().reduce(into: [Int](), {
            guard var intNumber = Int(String($1)) else { fatalError() }
            intNumber =  $0.count % 2 == 1 ? intNumber * 2 : intNumber
            if intNumber > 9 {
                intNumber = intNumber / 10 + intNumber % 10
            }
            $0.append(intNumber)
        })
        
        let sum = revesedNum.reduce(0, +)
        let result =  sum > 0 && sum % 10  == 0
        
        delegate?.cardNumberDidValidated(result ? .valid : .invalid, result ? nil : CVError.invalidCardNumber)
    }
}
