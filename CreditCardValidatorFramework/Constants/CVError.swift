//
//  CVError.swift
//  CreditCardValidatorFramework
//
//  Created by Vladimir. Evstratov on 12/26/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

public enum CVError: Error {
    case badURL
    case wrongMIMEType
    case invalidNumberLength
    case invalidCardNumber
    case wrongEntry
    case zeroEntry
}

extension CVError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .badURL:
            return CVStrings.badUrlError
        case .wrongMIMEType:
            return CVStrings.wrongMIMEType
        case .invalidNumberLength:
            return CVStrings.invalidLength
        case .invalidCardNumber:
            return CVStrings.invalidCardNumber
        case .wrongEntry:
            return CVStrings.wrongEntry
        case .zeroEntry:
            return CVStrings.zeroEntry
        }
    }
}
