//
//  CardValidatorStrings.swift
//  CreditCardValidatorFramework
//
//  Created by Vladimir. Evstratov on 12/26/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

class CVStrings {
    static let badUrlError = "Bad url"
    static let wrongMIMEType = "Wrong MIME type"
    static let invalidLength = "Invalid card number length"
    static let invalidCardNumber = "Invalid card number"
    static let wrongEntry = "Only digits and spaces are alowed"
    static let zeroEntry = "Number cannot start with 0 or space"
}
