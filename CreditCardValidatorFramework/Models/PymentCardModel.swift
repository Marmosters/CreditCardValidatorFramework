//
//  PaymentCardModel.swift
//  CreditCardValidatorFramework
//
//  Created by Vladimir. Evstratov on 12/25/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

/// Card number length details
public struct CardNumberDetails: Codable {
    /// card length. If it is nil  length can be from 12 to 19 digits
    public let length: Int?
}

/// country details
public struct CardCountry: Codable {
    /// country ID
    public let countryID: String?
    /// country code
    public let countryCode: String?
    /// country name
    public let countryName: String?
    /// country flag as emoji
    public let countryFlag: String?
    /// card currency
    public let currency: String?
    
    enum CodingKeys: String, CodingKey {
        case countryID = "numeric"
        case countryCode = "alpha2"
        case countryName = "name"
        case countryFlag = "emoji", currency
    }
}

/// issuer details
public struct CardIssuer: Codable {
    /// Bank name
    public let name: String?
    /// Bank site url
    public let url: String?
    /// Bank phone number
    public let phone: String?
    
    enum  CodingKeys: String, CodingKey {
        case name, url, phone
    }
}

/// This structure contains indormation about card  and issuer
public struct PaymentCardModel: Codable {
    /// Card number length details
    public let numberDetails: CardNumberDetails?
    /// Payment system
    public let scheme: String?
    /// Debet / credit
    public let type: String?
    /// Prepaid
    public let prepaid: Bool?
    /// issuer details
    public let bank: CardIssuer?
    /// country details
    public let country: CardCountry?
        
    enum CodingKeys: String, CodingKey {
        case numberDetails = "number", scheme, type, prepaid, bank, country
    }
}
