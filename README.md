The purpose of this framework is to evaluate if provided string can be a payment 
card number or not. In additition you can get issuer and card details if at least 
6 digits provided.

Usage:
1. Add framework to yout app
2. Create instanse of CardEvaluator class
3. Confirm CardEvaluatorDelegate protocol and assign delegate
4. Assign any string you want to evaluate as card number to CardEvaluator number 
   property and delegate methods to be calld to deliver evaluation result. 
**Strings starts with non-zero digit and contains only digits and spaces can be evaluated as card number**


To play with this framwork I made small demo app.Perfor steps below to run it
1. Clone [Demo app](https://gitlab.com/Marmosters/creditcardvalidator.git) and 
   this repository into same folder.
2. Navigate to the 'creditcardvalidator' folder 
3. Open CardValidator workspace and build it


Interface: 
```
public class CardEvaluator {
    */// delegate receives number evaluation status*
    public weak var delegate: CardValidatorDelegate?
    
    */// - Returns: Expected card number length*
    public var validNumberLehgths: ClosedRange<Int>
    
    */// This  variable acceps string entry to be evalueted as card number.*
    */// Evaluation starts  by value assigment and result will be delivered via*
    */// CardValidatorDelegate protocol methods.*
    public var cardNumber: String?
    
    public init()
}
```


```
public protocol CardEvaluatorDelegate: class {
    /// - description
    /// notify delegate about  starting  fetchung issuer data
    func cardDetailsWillUpdated()
    
    ///- description
    /// notify delegate about  finishing fetching issuer data
    /// - Parameters
    /// `details` - Issuer data
    /// `error` - fetch error
    ///  Possible errors:
    ///  - Network provided erros
    ///  - `CVError.badURL` - issus wit building request url
    ///  - `CVError.wrongMIMEType` - server returnned somthing, but not JSON
    func cardDetailsDidUpdated(_ details: PaymentCardModel?, _ error: Error?)
    
    ///- description
    /// notify delegate about  number evaluation results
    /// - Parameters
    /// `result` - validation result
    ///    Possible values:
    ///    - `CardValidationResult.valid` - evaluation passed
    ///    - `CardValidationResult.invalid` - evaluation failed
    ///    - `CardValidationResult.undefined` - nothing to evaluate
    /// `error` - clarification of negative evaluation
    ///  Possible errors:
    ///  - `CVError.invalidNumberLength` - provided string is not a card number becouse ot lenth
    ///  - `CVError.invalidCardNumber` - provided strinf did not passed Luhn evaluation
    ///  - `CVError.wrongEntry` - provided string conrains restricted characters. **Only digits and spaces are allowed**
    ///  - `CVError.zeroEntry` - provided string starts with 0 or space. **Leading 0 and spaces are not allowed**
    func cardNumberDidValidated(_ result: CardValidationResult, _ error: Error?)
}
```


Data models: 
```
/// This structure contains indormation about card  and issuer
public struct PaymentCardModel {
    /// Card number length details
    public let numberDetails: CardNumberDetails?
    /// Payment system
    public let scheme: String?
    /// Debet / credit
    public let type: String?
    /// Prepaid
    public let prepaid: Bool?
    /// issuer details
    public let bank: CardIssuer?
    /// country details
    public let country: CardCountry?
}
```


```
/// issuer details
public struct CardIssuer {
    /// Bank name
    public let name: String?
    /// Bank site url
    public let url: String?
    /// Bank phone number
    public let phone: String?
}
```


```
/// country details
public struct CardCountry: Codable {
    /// country ID
    public let countryID: String?
    /// country code
    public let countryCode: String?
    /// country name
    public let countryName: String?
    /// country flag as emoji
    public let countryFlag: String?
    /// card currency
    public let currency: String?
}
```


```
/// Card number length details
public struct CardNumberDetails {
    public let length: Int?
}
```


```
/// Card number length details
public struct CardNumberDetails {
    /// card length. If it is nil  length can be from 12 to 19 digits
    public let length: Int?
}
```


